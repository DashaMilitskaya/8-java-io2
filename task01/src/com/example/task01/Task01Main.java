package com.example.task01;

import lombok.NonNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.Optional;

public class Task01Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(extractSoundName(new File("src/main/resources/3727.mp3")));

    }

    public static String extractSoundName(@NonNull File file) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("src/main/resources/ffprobe",
                        "-v", "error", "-of", "flat", "-show_format", file.getPath())

                .redirectOutput(Redirect.PIPE)
                .redirectError(Redirect.INHERIT);
        Process process = processBuilder.start();
        try (
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(process.getInputStream()))
        ) {
            Optional<String> soundLine = reader.lines()
                    .filter(line -> line.startsWith("format.tags.title"))
                    .findFirst();

            String soundName = soundLine.orElse("Sound = \"error\"").split("=")[1];
            return soundName.substring(1, soundName.length() - 1);

        }

    }
}
