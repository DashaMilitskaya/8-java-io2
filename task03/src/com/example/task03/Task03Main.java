package com.example.task03;

import lombok.NonNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class Task03Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(deserialize(new FileInputStream("task03/src/main/resources/example1.bin")));


    }

    public static SampleData deserialize(@NonNull InputStream inputStream) throws IOException, ClassNotFoundException {
        // your implementation here
        SampleData deserializedObject;
        try(ObjectInputStream ois = new ObjectInputStream(inputStream)){
            deserializedObject = (SampleData) ois.readObject();
        }

        return deserializedObject;
    }
}
