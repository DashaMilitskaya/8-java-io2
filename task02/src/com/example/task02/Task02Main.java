package com.example.task02;

import lombok.NonNull;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class Task02Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(listFiles(Paths.get("task02/src/main/resources/")));


    }

    public static List<Path> listFiles(@NonNull Path rootDir) throws IOException, InterruptedException {
        List<Path> paths = new LinkedList<Path>();

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(rootDir)) {
            for (Path file : directoryStream) {
                if (file.toFile().isDirectory()) {
                    //paths.add(file);UNCOMMENT to out directories
                    paths.addAll(listFiles(file));
                } else {
                    paths.add(file);
                }
            }
        }
        return paths;
    }
}
